# terraform-multi-environment-vars

This project illustrates a common pattern in Terraform where each `environment` can be configured
via its own `.tfvars` file. However, the Terraform code itself is the same.

The GitLab integration is based on the GitLab-managed Terraform State and the
Terraform CI/CD Templates, see [the docs here](https://docs.gitlab.com/ee/user/infrastructure/iac/).

See [terraform-multi-environment](https://gitlab.com/timofurrer/terraform-multi-environment) for a similar 
approach where each `environment` has its own dedicated directory.